// Initiailisation des variables

// La sidenav
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);
});


// Initialisation du carousel
$(document).ready(function(){
    $('.carousel').carousel({
        fullWidth: true,
        indicators: true
    });
    setInterval(function f(){$('.carousel').carousel('next')}, 7000);

});

// dropdown
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var options = {
        hover: true
    };
    var instances = M.Dropdown.init(elems, options);
});


// Initialisation du material box
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});


// Initialisation du parallax
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems);
});